################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../adcsinglechannel.cfg 

CMD_SRCS += \
../MSP_EXP432P401R.cmd 

C_SRCS += \
../ControlBateria.c \
../MSP_EXP432P401R.c 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

C_DEPS += \
./ControlBateria.d \
./MSP_EXP432P401R.d 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./ControlBateria.obj \
./MSP_EXP432P401R.obj 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

OBJS__QUOTED += \
"ControlBateria.obj" \
"MSP_EXP432P401R.obj" 

C_DEPS__QUOTED += \
"ControlBateria.d" \
"MSP_EXP432P401R.d" 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

C_SRCS__QUOTED += \
"../ControlBateria.c" \
"../MSP_EXP432P401R.c" 


